<?php
require_once('animal.php');
require_once('ape.php');
require_once('frog.php');

echo "Names : ";
$sheep = new Animal("shaun");
echo "<br>Legs : ".$sheep->legs; // 4
echo "<br>Cold Blooded : ".$sheep->cold_blooded;

echo "<br><br>Names : ";
$sungokong = new Ape("kera sakti");
echo "<br>Legs : ".$sungokong->legs;
echo "<br>Cold Blooded : ".$sungokong->cold_blooded;
echo "<br>Yell : ";
echo $sungokong->yell(); // "Auooo"

echo "<br><br>Names : " ;
$kodok = new Frog("buduk");
echo "<br>Legs : ".$kodok->legs;
echo "<br>Cold Blooded : ".$kodok->cold_blooded; 
echo "<br>Jump : "; 
echo $kodok->jump() ; // "hop hop"
?>